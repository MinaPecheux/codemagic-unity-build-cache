using UnityEngine;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] private GameObject _target;

    void Start()
    {
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => {
            _target.SetActive(!_target.activeSelf);
        });
    }
}
