# [Unity/C#] Build cache tests with Codemagic

**Mina Pêcheux - July 2022**

![Codemagic build status](https://api.codemagic.io/apps/62e0ec9f097606f227bb662f/unity-win-workflow-cached/status_badge.svg)

---

This project is a small sample to show [Codemagic](https://unitycicd.com/)'s cache features to speed up your Unity project builds!

To test the building of the Unity project with and without cache locally, you can run the example `build-local-mac.sh` script (or adapt it for a Windows environment).

The process is as follows:

1. run `get-license-activation.sh` to get a `.alf` Unity file
2. make sure you're connected to Unity (at https://id.unity.com/)
3. upload `.alf` at: https://license.unity3d.com/manual, follow the instructions and get back your `.ulf` file
4. if need be, update the variables at the top of the `build-local-mac.sh` file to properly set the path to your Unity executable and your `.ulf` license file.
5. run: `sh build-local-mac.sh`
