#!/bin/sh

display_usage() {
  echo ""
  echo "usage :"
  echo "$0 [--help] [--no_cache]"
  echo ""
  echo "--no_cache: remove the project's cache before building"
  echo ""
}

USE_CACHE=1
UNITY_HOME=/Applications/Unity/Hub/Editor/2020.3.29f1/Unity.app
UNITY_BIN=$UNITY_HOME/Contents/MacOS/Unity
LICENSE_FILE=$(pwd)/Unity_v2020.x.ulf

for key in "$@"
do

case $key in
  --help|-h)
  echo help
  display_usage
  exit 0
  shift
  ;;
  --no_cache)
  USE_CACHE=0
  shift
  ;;
  *)    # unknown option
  shift
  ;;
esac
done

if [ $USE_CACHE -eq 0 ]; then
  # remove cache
  rm -r Library/
  rm -r Logs/
  rm -r obj/
  rm -r UserSettings/
fi

# remove previous build
rm -r mac

start=`date +%s`

echo "Building..."

$UNITY_BIN -batchmode -manualLicenseFile $LICENSE_FILE -logfile
$UNITY_BIN -batchmode -quit -logFile build-local.log -projectPath . -executeMethod BuildScript.BuildMac -nographics

end=`date +%s`

echo "Execution time: `expr $end - $start` sec"
