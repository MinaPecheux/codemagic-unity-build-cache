#!/bin/sh

UNITY_HOME=/Applications/Unity/Hub/Editor/2020.3.29f1/Unity.app
UNITY_BIN=$UNITY_HOME/Contents/MacOS/Unity

$UNITY_BIN -batchmode -createManualActivationFile -logfile

echo "1. Make sure you're connected (at https://id.unity.com/)"
echo "2. Upload .alf at: https://license.unity3d.com/manual"
